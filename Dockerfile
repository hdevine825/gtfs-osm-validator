FROM ubuntu:20.04

# disable interactive functions
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    # install needed packages
    apt-get install -y apt-utils && \
    apt-get install -y --no-install-recommends \
      less vim curl wget python3-pip gcc g++ libpython3-dev && \
    # upgrade OS
    apt-get -y dist-upgrade && \
    apt-get autoremove -y && \
    apt-get clean all

COPY requirements.txt /tmp/requirements.txt
RUN pip install --upgrade pip && \
    pip install -r /tmp/requirements.txt && \
    rm /tmp/requirements.txt

COPY src /opt/gtfs-osm-validator/src
WORKDIR /opt/gtfs-osm-validator/
