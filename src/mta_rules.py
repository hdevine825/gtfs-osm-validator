# -*- coding: utf-8 -*-

abbr = {
    'STREET': 'ST',
    'AVENUE': 'AV',
    'PLACE': 'PL',
    'SAINT': 'ST',
    'ROAD': 'RD',
    'FORT': 'FT',
    '1ST': '1',
    '2ND': '2',
    '3RD': '3',
    '4TH': '4',
    '5TH': '5',
    '6TH': '6',
    '7TH': '7',
    '8TH': '8',
    '9TH': '9'}


def normalize_gtfs_name(s):
    s = s.replace("/", " & ")
    s = s.replace("  ", " ")  # GTFS has lot of issues like this
    return s


def normalize_osm_name(s):
    s = s.upper()
    s = s.replace("'", "")
    for k, v in abbr.items():
    	s = s.replace(k,v)
    return s
